FROM openjdk:8-jre

# copy whole program to image
COPY target/iNotify-0.0.1-SNAPSHOT.jar iNotify-0.0.1-SNAPSHOT.jar

CMD ["java","-jar","iNotify-0.0.1-SNAPSHOT.jar"]
