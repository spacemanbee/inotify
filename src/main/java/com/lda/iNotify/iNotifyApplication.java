package com.lda.iNotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class iNotifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(iNotifyApplication.class, args);
	}
	
}
