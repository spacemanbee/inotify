package com.lda.iNotify.dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;

import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.lda.iNotify.common.TimeTools;
import com.lda.iNotify.model.CheckTerm;
import com.lda.iNotify.model.FriendList;
import com.lda.iNotify.model.INotifyUser;

@Component
public class CheckTermDao {

	@Resource
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Resource
	private JdbcTemplate jdbcTemplate;

	public List<CheckTerm> getPage(long userId, int page, int pageSize, int limit) {
		String sql = "SELECT * FROM check_term where host_user_id=" + userId;
		RowMapper<CheckTerm> rowMapper = new BeanPropertyRowMapper<>(CheckTerm.class);
		List<CheckTerm> chekterms = namedParameterJdbcTemplate.query(sql, rowMapper);
		if (chekterms.size() > 0) {
			System.out.println("getPage=" + chekterms.get(0).toString());
		}

		return chekterms;
	}

	public List<CheckTerm> getPagesByUserIds(List<Long> friendIds, int page, int pageSize, int limit) {
		
		String sql = "SELECT * FROM check_term where host_user_id in (" + friendIds.toString().substring(1, friendIds.toString().length()-1) +")";
		RowMapper<CheckTerm> rowMapper = new BeanPropertyRowMapper<>(CheckTerm.class);
		List<CheckTerm> chekterms = namedParameterJdbcTemplate.query(sql, rowMapper);
		if (chekterms.size() > 0) {
			System.out.println("getPagesByUserIds=" + chekterms.get(0).toString());
		}

		return chekterms;
	}
//	新增的部分2018-12-26
	public CheckTerm get(long userId, long id) {
		String sql = "SELECT * FROM check_term where id=:id AND host_user_id=:userId";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("userId", userId);
		paramMap.put("id", id);
		RowMapper<CheckTerm> rowMapper = new BeanPropertyRowMapper<>(CheckTerm.class);
		CheckTerm ct = namedParameterJdbcTemplate.queryForObject(sql, paramMap, rowMapper);
		System.out.println("get=" + ct.toString());
		return ct;
	}

	public void add(long userId, String content, String triggerTime, String triggerWeekday, String triggerDate, String triggerMonth, String triggerMonthDate, boolean active) {
//	public void add() {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("content", content);
		paramMap.put("triggerTime", triggerTime);
		paramMap.put("triggerWeekday", triggerWeekday);
		paramMap.put("triggerDate", triggerDate);
		paramMap.put("triggerMonth", triggerMonth);
		paramMap.put("triggerMonthDate", triggerMonthDate);
		String sql = "INSERT INTO check_term (host_user_id, content, trigger_time, trigger_weekday, trigger_date, trigger_month, "
				+ "trigger_month_date, active) VALUES ("+userId+",:content,:triggerTime,:triggerWeekday,"
				+ ":triggerDate,:triggerMonth,:triggerMonthDate,"+active+")";
		namedParameterJdbcTemplate.update(sql, paramMap);
	}

	public void update(long userId, long checkId, String content, String triggerTime, String triggerWeekday, String triggerDate, String triggerMonth, String triggerMonthDate, boolean active) {
		String UPDATE_SQL = "update check_term set active=" + active +",";
		Map<String, String> paramMap = new HashMap<>();

		if (content != null){
			paramMap.put("content", content);
			UPDATE_SQL += "content=:content,";
		}
		if(triggerTime != null){
			paramMap.put("triggerTime", triggerTime);
			UPDATE_SQL += "trigger_time=:triggerTime,";
		}
		if(triggerWeekday != null){
			paramMap.put("triggerWeekday", triggerWeekday);
			UPDATE_SQL += "trigger_weekday=:triggerWeekday,";
		}			
		if(triggerDate != null){
			paramMap.put("triggerDate", triggerDate);
			UPDATE_SQL += "trigger_date=:triggerDate,";
		}
		if(triggerMonth != null){
			paramMap.put("triggerMonth", triggerMonth);
			UPDATE_SQL += "trigger_month=:triggerMonth,";
		}
		if(triggerMonthDate != null){
			paramMap.put("triggerMonthDate", triggerMonthDate);
			UPDATE_SQL += "trigger_month_date=:triggerMonthDate,";
		}
		UPDATE_SQL = UPDATE_SQL.substring(0, UPDATE_SQL.length() - 1);
		UPDATE_SQL += " where host_user_id=" + userId + " AND id=" + checkId;
		namedParameterJdbcTemplate.update(UPDATE_SQL, paramMap);
	}

	public void delete(long userId, long checkId) {
		String DELETE_SQL = "delete from check_term where host_user_id =:userId AND id=:checkId";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("userId", userId);
		paramMap.put("checkId", checkId);
		namedParameterJdbcTemplate.update(DELETE_SQL, paramMap);
	}

}
