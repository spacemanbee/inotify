package com.lda.iNotify.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;

import com.lda.iNotify.model.FriendList;

@Component
public class FriendListDao {

	@Resource
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Resource
	private JdbcTemplate jdbcTemplate;

	public FriendList get(long Id) {
		String sql = "SELECT * FROM friend_list where id=:id";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("id", Id);
		RowMapper<FriendList> rowMapper = new BeanPropertyRowMapper<>(FriendList.class);
		FriendList friendList = namedParameterJdbcTemplate.queryForObject(sql, paramMap, rowMapper);
		System.out.println("get=" + friendList.toString());
		return friendList;
	}

	public List<FriendList> getFriends(long userId, int pageSize, int limit) {
		String sql = "SELECT * FROM friend_list limit "+ limit;
		RowMapper<FriendList> rowMapper = new BeanPropertyRowMapper<>(FriendList.class);
		List<FriendList> friendLists = namedParameterJdbcTemplate.query(sql, rowMapper);
		System.out.println("getPage=" + friendLists.get(0).toString());
		return friendLists;
	}

	public void insert() {
		Map<String, Integer> paramMap = new HashMap<>();
		paramMap.put("host_user_id", 2);
		paramMap.put("friend_id", 4);
		String sql = "insert into friend_list (host_user_id, friend_id) values (:host_user_id,:friend_id)";
		namedParameterJdbcTemplate.update(sql, paramMap);
	}

	public void update() {
		String UPDATE_SQL = "update friend_list set host_user_id=:hostUserId,friend_id=:friendId where id=:id";
		FriendList friendList = new FriendList();
		friendList.setId(2);
		friendList.setHostUserId(55);
		friendList.setFriendId(66);
		SqlParameterSource ps = new BeanPropertySqlParameterSource(friendList);
		namedParameterJdbcTemplate.update(UPDATE_SQL, ps);
	}

	public void delete() {
		String DELETE_SQL = "delete from friend_list where id=:id";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("id", 2L);
		namedParameterJdbcTemplate.update(DELETE_SQL, paramMap);
	}

	public void create(long hostUserId, long friendId) throws Exception {

		if (exist(hostUserId, friendId)) {
			throw new Exception("Already added");
		}
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("host_user_id", hostUserId);
		paramMap.put("friend_id", friendId);
		String sql = "insert into friend_list (host_user_id, friend_id) values (:host_user_id,:friend_id)";
		namedParameterJdbcTemplate.update(sql, paramMap);
	}

	public void delete(long hostUserId, long friendId) {
		String DELETE_SQL = "delete from friend_list where host_user_id=:host_user_id and friend_id=:friend_id";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("host_user_id", hostUserId);
		paramMap.put("friend_id", friendId);
		namedParameterJdbcTemplate.update(DELETE_SQL, paramMap);
	}

	public int count(long hostUserId, long friendId) {
		String sql = "select count(*) from friend_list where host_user_id=" + hostUserId + " and friend_id=" + friendId;
		return jdbcTemplate.queryForObject(sql, new Object[] {}, Integer.class);
	}

	public boolean exist(long hostUserId, long friendId) {
		return count(hostUserId, friendId) > 0 ? true : false;
	}
}
