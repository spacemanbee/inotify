package com.lda.iNotify.dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.lda.iNotify.common.TimeTools;
import com.lda.iNotify.model.INotifyUser;

@Component
public class UserDao {

	@Resource
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Resource
	private JdbcTemplate jdbcTemplate;

	public INotifyUser get(long Id) {
		String sql = "SELECT * FROM user where id=:id";
		Map<String, Long> paramMap = new HashMap<>();
		paramMap.put("id", Id);
		RowMapper<INotifyUser> rowMapper = new BeanPropertyRowMapper<>(INotifyUser.class);
		INotifyUser user = namedParameterJdbcTemplate.queryForObject(sql, paramMap, rowMapper);
		System.out.println("get=" + user.toString());
		return user;
	}

	public INotifyUser getByUniqueId(String uniqueId) {
		String sql = "SELECT * FROM user where unique_id=:uniqueId";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("uniqueId", uniqueId);
		RowMapper<INotifyUser> rowMapper = new BeanPropertyRowMapper<>(INotifyUser.class);
		INotifyUser user = namedParameterJdbcTemplate.queryForObject(sql, paramMap, rowMapper);
		System.out.println("get=" + user.toString());
		return user;
	}

	public INotifyUser getByToken(String token) {
		String sql = "SELECT * FROM user where token=:token";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("token", token);
		RowMapper<INotifyUser> rowMapper = new BeanPropertyRowMapper<>(INotifyUser.class);
		INotifyUser user = namedParameterJdbcTemplate.queryForObject(sql, paramMap, rowMapper);
		System.out.println("get=" + user.toString());
		return user;
	}

	public INotifyUser create(String nickName, String fbId) throws NoSuchAlgorithmException {
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("nick_name", nickName);
		Date now = TimeTools.getTaipeiNowDate();
		String unique_id = String.valueOf(now.getTime());
		String sql = "INSERT INTO `user` (`nick_name`, `fb_id`, `unique_id`, `token`, `create_time`, `last_login_time`)\n"
				+ "VALUES\n" + "	(:nick_name, '" + fbId + "', '" + unique_id + "', '" + getTokenWithMd5(fbId)
				+ "', '" + now + "', '" + now + "', 16);";
		namedParameterJdbcTemplate.update(sql, paramMap);

		return getByUniqueId(unique_id);
	}

	private String getTokenWithMd5(String fbId) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(fbId.getBytes());
		byte[] digest = md.digest();
		return DatatypeConverter.printHexBinary(digest).toUpperCase();
	}

	public int countByFbId(String fbId) {
		String sql = "select count(*) from user where fb_id=" + fbId;
		return jdbcTemplate.queryForObject(sql, new Object[] {}, Integer.class);
	}

	public INotifyUser findByFbId(String fbId) {
		String sql = "SELECT * FROM user where fb_id=:fbId";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("fbId", fbId);
		RowMapper<INotifyUser> rowMapper = new BeanPropertyRowMapper<>(INotifyUser.class);
		INotifyUser user = namedParameterJdbcTemplate.queryForObject(sql, paramMap, rowMapper);
		System.out.println("get=" + user.toString());
		return user;
	}

	public void update(INotifyUser iNotifyUser) {
		String sql = "UPDATE user set nick_name=:nickName, can_search=" + iNotifyUser.isCanSearch()
				+ ",today_api_count=" + iNotifyUser.getTodayApiCount() + ",total_api_count="
				+ iNotifyUser.getTotalApiCount() + ", last_login_time=now() where id="
				+ iNotifyUser.getId();
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("nickName", iNotifyUser.getNickName());
		namedParameterJdbcTemplate.update(sql, paramMap);
	}

	public void updateUniqueId(long userId, String uniqueId) throws Exception {
		String sql = "select can_update_unique_id from user where id=?";
		boolean canUpdateUniqueId = jdbcTemplate.queryForObject(sql, new Long[] { userId }, Boolean.class);
		System.out.println("canUpdateUniqueId=" + canUpdateUniqueId);
		if (!canUpdateUniqueId) {
			throw new Exception("Already changed, can not change again");
		}

		try {
			sql = "UPDATE user set unique_id=:uniqueId, can_update_unique_id=false where id="+userId;
			Map<String, String> paramMap = new HashMap<>();
			paramMap.put("uniqueId", uniqueId);
			namedParameterJdbcTemplate.update(sql, paramMap);
		} catch (Exception e) {
			throw new Exception("Already exist, please change");
		}
	}
}
