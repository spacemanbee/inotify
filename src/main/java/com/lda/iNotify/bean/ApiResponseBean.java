package com.lda.iNotify.bean;
import org.springframework.stereotype.Service;

@Service("ApiResponseBean")
public class ApiResponseBean {

	private boolean success;
	private String message;

	public ApiResponseBean() {
		this.success = false;
	}
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}