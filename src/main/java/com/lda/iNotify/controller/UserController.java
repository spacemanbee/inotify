package com.lda.iNotify.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lda.iNotify.bean.ApiResponseBean;
import com.lda.iNotify.dao.UserDao;
import com.lda.iNotify.model.INotifyUser;

@RestController
@RequestMapping("/api/user")
public class UserController {

	public static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Resource
	private UserDao userDao;

	@RequestMapping(value = "/get", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean update(@RequestParam(value = "user_id", defaultValue = "0") long userId) {
		NovelUserApiResponseBean apiResponseBean = new NovelUserApiResponseBean();

		try {
			INotifyUser iNotifyUser = userDao.get(userId);
			apiResponseBean.setSuccess(true);
			apiResponseBean.setINotifyUser(iNotifyUser);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean update(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "nick_name", defaultValue = "") String nickName,
			@RequestParam(value = "can_search", defaultValue = "true") Boolean canSearch) {
		NovelUserApiResponseBean apiResponseBean = new NovelUserApiResponseBean();
		if (nickName.isEmpty()) {
			apiResponseBean.setMessage("nickName is empty");
			return apiResponseBean;
		}

		try {
			INotifyUser iNotifyUser = userDao.get(userId);
			iNotifyUser.setNickName(nickName);
			iNotifyUser.setCanSearch(canSearch);
			apiResponseBean.setSuccess(true);
			userDao.update(iNotifyUser);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/unique_id/update", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean update(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "unique_id", defaultValue = "") String uniqueId) {
		NovelUserApiResponseBean apiResponseBean = new NovelUserApiResponseBean();
		if (uniqueId.isEmpty()) {
			apiResponseBean.setMessage("uniqueId is empty");
			return apiResponseBean;
		}

		if (uniqueId.length() > 20) {
			apiResponseBean.setMessage("uniqueId too long, need less 20 chars");
			return apiResponseBean;
		}

		try {
			userDao.updateUniqueId(userId, uniqueId);
			// TODO: 讀寫分離後, 這邊要注意
			INotifyUser iNotifyUser = userDao.get(userId);
			apiResponseBean.setSuccess(true);
			apiResponseBean.setINotifyUser(iNotifyUser);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	private class NovelUserApiResponseBean extends ApiResponseBean {
		private INotifyUser iNotifyUser;

		public INotifyUser getINotifyUser() {
			if (iNotifyUser != null) {
				iNotifyUser.setId(0L);// hide userId
			}
			return iNotifyUser;
		}

		public void setINotifyUser(INotifyUser iNotifyUser) {
			this.iNotifyUser = iNotifyUser;
		}

	}
}
