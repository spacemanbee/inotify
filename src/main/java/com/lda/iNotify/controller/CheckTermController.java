package com.lda.iNotify.controller;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lda.iNotify.bean.ApiResponseBean;
//import com.lda.iNotify.controller.UserController.NovelUserApiResponseBean;
//import com.lda.iNotify.controller.UserController.NovelUserApiResponseBean;
//import com.lda.iNotify.controller.FriendController.FriendApiResponseBean;
import com.lda.iNotify.dao.CheckTermDao;
import com.lda.iNotify.dao.FriendListDao;
import com.lda.iNotify.dao.UserDao;
import com.lda.iNotify.model.CheckTerm;
import com.lda.iNotify.model.FriendList;
import com.lda.iNotify.model.INotifyUser;

@RestController
@RequestMapping("/api/chek-term")
public class CheckTermController {

	public static final Logger logger = LoggerFactory.getLogger(CheckTermController.class);

	@Resource
	private CheckTermDao checkTermDao;

	@Resource
	private UserDao userDao;

	@Resource
	private FriendListDao friendListDao;

	@RequestMapping(value = "", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean get(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "page", defaultValue = "0") int page) {
		CheckTermApiResponseBean apiResponseBean = new CheckTermApiResponseBean();
		if (page < 0) {
			apiResponseBean.setMessage("page < 0");
			return apiResponseBean;
		}

		int pageSize = 10;
		int limit = 10;
		try {
			List<CheckTerm> checkTerms = checkTermDao.getPage(userId, page, pageSize, limit);
			apiResponseBean.setSuccess(true);
			apiResponseBean.setCheckTerms(checkTerms);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/friend", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean friend(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "page", defaultValue = "0") int page) {
		CheckTermApiResponseBean apiResponseBean = new CheckTermApiResponseBean();
		if (page < 0) {
			apiResponseBean.setMessage("page < 0");
			return apiResponseBean;
		}

		int pageSize = 10;
		int limit = 10;
		try {
			List<Long> friendIds = friendListDao.getFriends(userId, pageSize, limit).stream()
					.map(FriendList::getFriendId).collect(Collectors.toList());
			List<CheckTerm> checkTerms = checkTermDao.getPagesByUserIds(friendIds, page, pageSize, limit);
			apiResponseBean.setSuccess(true);
			apiResponseBean.setCheckTerms(checkTerms);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	private class CheckTermApiResponseBean extends ApiResponseBean {
		private List<CheckTerm> checkTerms = new ArrayList<>();

		public List<CheckTerm> getCheckTerms() {
			return checkTerms;
		}

		public void setCheckTerms(List<CheckTerm> checkTerms) {
			this.checkTerms = checkTerms;
		}

	}
//以下是我新增的部分2018-12-21
	private class OneTermApiResponseBean extends ApiResponseBean {
		private CheckTerm checkTerm;

		public CheckTerm getCheckTerm() {
			if (checkTerm != null) {
//				checkTerm.setId(0L);// hide userId
			}
			return checkTerm;
		}

		public void setCheckTerm(CheckTerm ct) {
			this.checkTerm = ct;
		}

	}

	@RequestMapping(value = "/add", method = { RequestMethod.POST, RequestMethod.GET })
	public CheckTermApiResponseBean create(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "content", defaultValue = "") String content, 
			@RequestParam(value = "triggerTime", defaultValue = "00:00:00") String triggerTime, 
			@RequestParam(value = "triggerWeekday", defaultValue = "0000000") String triggerWeekday, 
			@RequestParam(value = "triggerDate", defaultValue = "1911-01-01") String triggerDate, 
			@RequestParam(value = "triggerMonth", defaultValue = "000000000000") String triggerMonth, 
			@RequestParam(value = "triggerMonthDate", defaultValue = "0000000000000000000000000000000") String triggerMonthDate, 
			@RequestParam(value = "active", defaultValue = "1") boolean active) {

		CheckTermApiResponseBean apiResponseBean = new CheckTermApiResponseBean();
		if (content.isEmpty()) {
			apiResponseBean.setMessage("content is empty");
			return apiResponseBean;
		}

		try {
		    checkTermDao.add(userId, content, triggerTime, triggerWeekday, triggerDate, triggerMonth, triggerMonthDate, active);
			apiResponseBean.setSuccess(true);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean delete(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "check_term_id", defaultValue = "0") long uniqueId) {
		CheckTermApiResponseBean apiResponseBean = new CheckTermApiResponseBean();
		if (uniqueId <= 0) {
			apiResponseBean.setMessage("uniqueId is wrong");
			return apiResponseBean;
		}

		try {
//			INotifyUser hostUser = userDao.get(userId);
//			INotifyUser addFriendUser = userDao.getByUniqueId(uniqueId);
			checkTermDao.delete(userId, uniqueId);
			apiResponseBean.setSuccess(true);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}
	
	@RequestMapping(value = "/get", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean update(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "check_term_id", defaultValue = "0") long checkId) {
		OneTermApiResponseBean apiResponseBean = new OneTermApiResponseBean();

		try {
			CheckTerm checkTerm = checkTermDao.get(userId, checkId);
			apiResponseBean.setSuccess(true);
			apiResponseBean.setCheckTerm(checkTerm);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}
	
	@RequestMapping(value = "/update", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean update(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "check_term_id", defaultValue = "0") long checkId,
			@RequestParam(value = "content", required = false) String content, 
			@RequestParam(value = "triggerTime", required = false) String triggerTime, 
			@RequestParam(value = "triggerWeekday", required = false) String triggerWeekday, 
			@RequestParam(value = "triggerDate", required = false) String triggerDate, 
			@RequestParam(value = "triggerMonth", required = false) String triggerMonth, 
			@RequestParam(value = "triggerMonthDate", required = false) String triggerMonthDate, 
			@RequestParam(value = "active", defaultValue = "1") boolean active) {
		CheckTermApiResponseBean apiResponseBean = new CheckTermApiResponseBean();
		if (checkId <= 0) {
			apiResponseBean.setMessage("check_term_id is empty");
			return apiResponseBean;
		}
		try {
			apiResponseBean.setSuccess(true);
			checkTermDao.update(userId, checkId, content, triggerTime, triggerWeekday, triggerDate, triggerMonth, triggerMonthDate, active);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

}
