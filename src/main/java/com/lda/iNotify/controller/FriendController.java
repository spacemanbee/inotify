package com.lda.iNotify.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lda.iNotify.bean.ApiResponseBean;
import com.lda.iNotify.dao.FriendListDao;
import com.lda.iNotify.dao.UserDao;
import com.lda.iNotify.model.INotifyUser;

@RestController
@RequestMapping("/api/friend")
public class FriendController {

	public static final Logger logger = LoggerFactory.getLogger(FriendController.class);

	@Resource
	private UserDao userDao;

	@Resource
	private FriendListDao friendListDao;

	@RequestMapping(value = "/add", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean create(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "unique_id", defaultValue = "") String uniqueId) {
		FriendApiResponseBean apiResponseBean = new FriendApiResponseBean();
		if (uniqueId.isEmpty()) {
			apiResponseBean.setMessage("uniqueId is empty");
			return apiResponseBean;
		}

		try {
			INotifyUser hostUser = userDao.get(userId);
			INotifyUser addFriendUser = userDao.getByUniqueId(uniqueId);
			friendListDao.create(hostUser.getId(), addFriendUser.getId());
			apiResponseBean.setSuccess(true);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean delete(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "unique_id", defaultValue = "") String uniqueId) {
		FriendApiResponseBean apiResponseBean = new FriendApiResponseBean();
		if (uniqueId.isEmpty()) {
			apiResponseBean.setMessage("uniqueId is empty");
			return apiResponseBean;
		}

		try {

			INotifyUser hostUser = userDao.get(userId);
			INotifyUser addFriendUser = userDao.getByUniqueId(uniqueId);
			friendListDao.delete(hostUser.getId(), addFriendUser.getId());
			apiResponseBean.setSuccess(true);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/find", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean findFriend(@RequestParam(value = "user_id", defaultValue = "0") long userId,
			@RequestParam(value = "unique_id", defaultValue = "") String uniqueId) {
		FriendApiResponseBean apiResponseBean = new FriendApiResponseBean();
		if (uniqueId.isEmpty()) {
			apiResponseBean.setMessage("uniqueId is empty");
			return apiResponseBean;
		}

		try {
			INotifyUser iNotifyUser = userDao.getByUniqueId(uniqueId);
			apiResponseBean.setSuccess(true);
			apiResponseBean.setINotifyUser(iNotifyUser);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	private class FriendApiResponseBean extends ApiResponseBean {
		private INotifyUser iNotifyUser;

		public INotifyUser getINotifyUser() {
			if (iNotifyUser != null) {
				iNotifyUser.setId(0L);// hide userId
			}
			return iNotifyUser;
		}

		public void setINotifyUser(INotifyUser iNotifyUser) {
			this.iNotifyUser = iNotifyUser;
		}

	}
}
