package com.lda.iNotify.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lda.iNotify.bean.ApiResponseBean;
import com.lda.iNotify.dao.UserDao;
import com.lda.iNotify.model.INotifyUser;

@RestController
@RequestMapping("/api/fb/user")
public class FbUserController {

	public static final Logger logger = LoggerFactory.getLogger(FbUserController.class);

	@Resource
	private UserDao userDao;

	@RequestMapping(value = "/create", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean create(@RequestParam(value = "nick_name", defaultValue = "") String nickName,
			@RequestParam(value = "fb_access_token", defaultValue = "") String fbAccessToken) {
		NovelUserApiResponseBean apiResponseBean = new NovelUserApiResponseBean();
		if (nickName.isEmpty() || nickName.length() > 20) {
			apiResponseBean.setMessage("nickName is empty or length > 20");
			return apiResponseBean;
		}
		if (fbAccessToken.isEmpty()) {
			apiResponseBean.setMessage("fbAccessToken is empty");
			return apiResponseBean;
		}

		try {
			String FbId = getFbUser(fbAccessToken).getId();
			boolean isNewUser = userDao.countByFbId(FbId) == 0 ? true : false;
			INotifyUser iNotifyUser = null;
			if (isNewUser) {
				iNotifyUser = userDao.create(nickName, FbId);
			} else {
				iNotifyUser = userDao.findByFbId(FbId);
			}
			apiResponseBean.setSuccess(true);
			apiResponseBean.setINotifyUser(iNotifyUser);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	@RequestMapping(value = "/exist", method = { RequestMethod.POST, RequestMethod.GET })
	public ApiResponseBean create(@RequestParam(value = "fb_access_token", defaultValue = "") String fbAccessToken) {
		NovelUserApiResponseBean apiResponseBean = new NovelUserApiResponseBean();
		if (fbAccessToken.isEmpty()) {
			apiResponseBean.setMessage("fbAccessToken is empty");
			return apiResponseBean;
		}

		try {
			String fbId = getFbUser(fbAccessToken).getId();
			boolean isNewUser = userDao.countByFbId(fbId) == 0 ? true : false;
			INotifyUser iNotifyUser = null;
			if (isNewUser) {
				apiResponseBean.setMessage("no user");
			} else {
				iNotifyUser = userDao.findByFbId(fbId);
			}
			apiResponseBean.setSuccess(true);
			apiResponseBean.setINotifyUser(iNotifyUser);
		} catch (Exception e) {
			apiResponseBean.setMessage(e.getMessage());
		}
		return apiResponseBean;
	}

	private User getFbUser(String fbAccessToken) {
		Facebook facebook = new FacebookTemplate(fbAccessToken);
		String[] fields = { "id", "email", "first_name", "last_name" };
		return facebook.fetchObject("me", User.class, fields);
	}

	private class NovelUserApiResponseBean extends ApiResponseBean {
		private INotifyUser iNotifyUser;

		public INotifyUser getINotifyUser() {
			if (iNotifyUser != null) {
				iNotifyUser.setId(0L);// hide userId
			}
			return iNotifyUser;
		}

		public void setINotifyUser(INotifyUser iNotifyUser) {
			this.iNotifyUser = iNotifyUser;
		}

	}
}
