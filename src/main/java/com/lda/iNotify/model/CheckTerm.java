package com.lda.iNotify.model;

import lombok.Data;

import java.sql.Time;
import java.util.Date;

@Data
public class CheckTerm {
	
	private Long id;
	private Long hostUserId;
	private String content;
	private Time triggerTime;
//	private Byte triggerWeekday;
	private String triggerWeekday;
	private Date triggerDate;
//	private long triggerMonth;
	private String triggerMonth;
//	private long triggerMonthDate;
	private String triggerMonthDate;
	private boolean active;
	private Date createTime;
}
