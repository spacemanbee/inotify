package com.lda.iNotify.model;

import lombok.Data;
import java.util.Date;

@Data
public class INotifyUser {
	
	private Long id;
	private String nickName;
	private String fbId;
	private String token;
	private String uniqueId;
	private int todayApiCount;
	private Date lastLoginTime;
	private double totalApiCount;
	private Date createTime;
	private boolean canSearch;
	private boolean canUpdateUniqueId;
}
