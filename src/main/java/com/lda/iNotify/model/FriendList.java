package com.lda.iNotify.model;

import java.util.Date;
import lombok.Data;

@Data
public class FriendList {
	private long id;
	private long hostUserId;
	private long friendId;
	private Date buildTime;
}