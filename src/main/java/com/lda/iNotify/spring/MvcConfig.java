package com.lda.iNotify.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@EnableWebMvc
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

	public MvcConfig() {
		super();
	}

	@Bean
	public RequestProcessingTimeInterceptor requestProcessingTimeInterceptor() {
		return new RequestProcessingTimeInterceptor();
	}

	@Override
	public void addInterceptors(final InterceptorRegistry registry) {
		registry.addInterceptor(requestProcessingTimeInterceptor());// 要用spring的bean, bean裡面才會幫忙autowired
	}
}
