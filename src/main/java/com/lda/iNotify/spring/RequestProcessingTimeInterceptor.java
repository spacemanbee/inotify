package com.lda.iNotify.spring;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.lda.iNotify.common.TimeTools;
import com.lda.iNotify.dao.UserDao;
import com.lda.iNotify.model.INotifyUser;

@Service("RequestProcessingTimeInterceptor")
public class RequestProcessingTimeInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(RequestProcessingTimeInterceptor.class);

	@Autowired
	private UserDao userDao;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
//		if (request.getMethod().equals("GET")) {
//			return true;
//		}
		if (request.getRequestURL().toString().contains("/api/fb/user")) { // create user, not have token now
			return true;
		}

		String inputToken = request.getParameter("token");
		if (inputToken == null || inputToken.isEmpty()) {
			response.getWriter().write("please use token");
			return false;
		}
		
		if (request.getParameter("user_id") == null) {
			response.getWriter().write("{\"error\":\" no user_id\"}");
			return false;
		}
		
		long inputUserId = Long.valueOf(request.getParameter("user_id"));
		INotifyUser user = userDao.get(inputUserId);

		if (user == null) {
			response.getWriter().write("no user");
			return false;
		}
		try {
			if (!inputToken.equals(user.getToken())) {
				response.getWriter().write("{\"error\":\"not match user_id\"}");
				return false;
			}
		} catch (Exception e) {
			response.getWriter().write("{\"error\":\"not match user_id\"}");
			return false;
		}
		Date userLastLogin = user.getLastLoginTime();
		int todayApiCount = user.getTodayApiCount()+1;
		if (userLastLogin.getDay() != TimeTools.getTaipeiNowDate().getDay()) {
			todayApiCount = 1;
		}
		user.setTodayApiCount(todayApiCount);
		user.setTotalApiCount(user.getTotalApiCount()+1);
		user.setLastLoginTime(TimeTools.getTaipeiNowDate());
		userDao.update(user);
		return true;
	}

}