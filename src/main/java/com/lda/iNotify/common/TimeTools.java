package com.lda.iNotify.common;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class TimeTools {

	public static Date getTaipeiNowDate() {
		Instant now = Instant.now();
		ZoneId zoneId = ZoneId.of("Asia/Taipei");
		ZonedDateTime dateAndTimeInLA = ZonedDateTime.ofInstant(now, zoneId);
		try {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateAndTimeInLA.toString().substring(0, 19).replace("T", " "));
		} catch (ParseException e) {
		}
		return null;
	}
}
