## API
user_id=30
token=0192F9D16EA8669EFCB73A2959507691

### Friend相關
#### 001 增加朋友
addFriend(String user_id, String token, String uniqueId)   
token: 使用者登入後取得的token   
uniqueId: 此user Unique Id   
ex:   
http://localhost:8080/api/friend/add?user_id=30&token=0192F9D16EA8669EFCB73A2959507691&unique_id=Flyerson   

#### 002 搜尋朋友(單獨 在加入朋友頁面上使用)
findFriend(String user_id, String token, String uniqueId)   
token: 使用者登入後取得的token   
uniqueId: 此user Unique Id   
ex:   
http://localhost:8080/api/friend/find?user_id=30&token=0192F9D16EA8669EFCB73A2959507691&unique_id=Flyerson   

#### 003 刪除朋友
deleteFriend(String user_id, String token, String uniqueId)   
token: 使用者登入後取得的token   
userId: 此user Unique Id   
ex:   
http://localhost:8080/api/friend/delete?user_id=30&token=0192F9D16EA8669EFCB73A2959507691&unique_id=Flyerson   

### Profile相關
#### 004 更新自己的profile (目前創建只能透過臉書登入)
updateProfile(String user_id, String token, String nickName, Boolean canSearch)   
token: 使用者登入後取得的token   
nickName: 匿名 可以更改   
canSearch: 是否可以被搜尋到 (defalut = true)   
ex:   
http://localhost:8080/api/user/update?user_id=30&token=0192F9D16EA8669EFCB73A2959507691&nick_name=Bee&can_search=false   

#### 005 得到自己的Profile
getProfile(String user_id,String token)   
token: 使用者登入後取得的toke n   
ex:   
http://localhost:8080/api/user/get?user_id=30&token=0192F9D16EA8669EFCB73A2959507691   

#### 006 修改自己的 uniqueId on Profile (只能修改一次)
editProfileUniqueId(String user_id, String token, String uniqueId)      
token: 使用者登入後取得的token   
uniqueId: 此user unique Id就像line id一樣 (maxLength=20)      
ex:   
http://localhost:8080/api/user/unique_id/update?user_id=30&token=0192F9D16EA8669EFCB73A2959507691&unique_id=Bee   

### Self Notify ListView (顯示在Profile那頁)
#### 007 獲得自己的Notify ListView
getSlefNotifyList(String user_id, String token, int page)   
token: 使用者登入後取得的token  
page: 翻頁數   
ex:   
http://localhost:8080/api/chek-term?user_id=30&token=0192F9D16EA8669EFCB73A2959507691   

### Home Notify ListView (首頁相關)
#### 007 得到官方首頁列表
getOfficialNotifyList(String token, int page)   
token: 使用者登入後取得的token    
page: 翻頁數   

#### 008 得到朋友列表
getFriendNotifyList(String token, int page)   
token: 使用者登入後取得的token    
page: 翻頁數   
ex:   
http://localhost:8080/api/chek-term/friend?user_id=30&token=0192F9D16EA8669EFCB73A2959507691   

### 發 notification 的時間規則   
### (一開始的level 1 流程是 加完好友後, 可以發訊息給朋友)   
   
#### 030 取得check term詳細資料   
輸入以下測試網址會得到一個json(用"&"把項目串接起來)   
http://localhost:8080/api/chek-term/get?token=0192F9D16EA8669EFCB73A2959507691&user_id=30&check_term_id=7   
必要輸入項目   
1. token   
2. user_id   
3. check_term_id   
   
#### 031 增加check item (先用簡單版，跟手機鬧鐘一樣)   
測試用API    http://localhost:8080/api/chek-term/add?token=0192F9D16EA8669EFCB73A2959507691&user_id=30&content=beeeeee&active=0   
必要輸入項目   
1. token   
2. user_id   
3. content(不可為空字串)   
非必要輸入   
1. triggerTime=13:26:00就是指下午一點26分發出提醒(hour:min:second的格式)   
2. triggerWeekday=0010001就是指每週一和週五(七個char)   
3. triggerDate=2018-12-26就是指12-26該天會發出提醒(yyyy-mm-dd的格式)   
4. triggerMonth=100000010000就是指每年的5月和12月(長度為12個char)   
5. triggerMonthDate=0000000000000000000000000000010就是指每個月的2號(長度為31個char)   
6. active=1為啟用此check term(預設), active=0為關閉   
   
#### 032 更新check term   
http://localhost:8080/api/chek-term/update?token=0192F9D16EA8669EFCB73A2959507691&user_id=30&check_term_id=1   
必要輸入項目   
1. token   
2. user_id   
3. content(不可為空字串)   
4. check_term_id   
非必要輸入   
1. triggerTime=13:26:00就是指下午一點26分發出提醒(hour:min:second的格式)   
2. triggerWeekday=0010001就是指每週一和週五(七個char)   
3. triggerDate=2018-12-26就是指12-26該天會發出提醒(yyyy-mm-dd的格式)   
4. triggerMonth=100000010000就是指每年的5月和12月(長度為12個char)   
5. triggerMonthDate=0000000000000000000000000000010就是指每個月的2號(長度為31個char)   
6. active=1為啟用此check term, active=0為關閉   
   
#### 033 刪除check item   
http://localhost:8080/api/chek-term/delete?token=0192F9D16EA8669EFCB73A2959507691&user_id=30&check_term_id=8   
必要輸入項目   
1. token   
2. user_id   
3. check_term_id     

#### 034 增加能看到check item的朋友   
addFriendToItem(String token, int itemIdx, String[] uniqueId)   
token: 使用者登入後取得的token    
itemIdx: 欲修改的item的index   
uniqueId: 此朋友user Unique Id   

#### 035 刪除能看到check item的朋友   
delFriendFromItem(String token, int itemIdx, String[] uniqueId)   
token: 使用者登入後取得的token    
itemIdx: 欲修改的item的index   
uniqueId: 此朋友user Unique Id   
